#define BOOST_TEST_MODULE LightConfigTest
#include <boost/test/unit_test.hpp>

#include "LightConfig.h"

BOOST_AUTO_TEST_CASE(basic_test) {
	Show show;
	show.addFixture(1);
	show.addCuelist();
	show.setCueList(0).addCue().setEffect() = 1.f;
	show.setCueList(0).addCue().setEffect() = 0.5f;

	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 1.f);
	BOOST_TEST(show.getCueLists()[0]->getCue(1).getEffect() == 0.5f);
}

BOOST_AUTO_TEST_CASE(undo_redo_test) {
	Show show;
	show.addCuelist().addCue().setEffect() = 1.f;
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 1.f);

	// change a value of the configuration
	show.setCueList(0).setCue(0).setEffect() = 0.5f;
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.5f);

	show.undo();
	// expect the original value
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 1.f);

	show.redo();
	//expect the "re-done" change again
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.5f);

	show.setCueList(0).setCue(0).setEffect() = 0.1f;
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.1f);
	show.setCueList(0).setCue(0).setEffect() = 0.2f;
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.2f);

	// we can undo multiple changes.
	show.undo();
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.1f);
	show.undo();
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.5f);

	// and we can redo multiple changes again.
	show.redo();
	show.redo();
	BOOST_TEST(show.getCueLists()[0]->getCue(0).getEffect() == 0.2f);
}

BOOST_AUTO_TEST_CASE(test_adding_elements) {
	Show show;
	show.addFixture(1);
	show.addFixture(2);

	BOOST_TEST(*show.getFixtures()[0] == 1);
	BOOST_TEST(*show.getFixtures()[1] == 2);
	show.undo();
	show.addFixture(3);
	BOOST_TEST(*show.getFixtures()[1] == 3);
}