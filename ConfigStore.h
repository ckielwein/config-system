#ifndef LIGHTCONFIG_CONFIGSTORE_H
#define LIGHTCONFIG_CONFIGSTORE_H

#include <memory>
#include <vector>

/// Returns a mutable pointer to the copy of the object stored in @p o
template<class T>
[[nodiscard]] auto deep_copy(std::shared_ptr<const T> o) {
	return std::make_shared<T>(*o);
}

/// replace the pointer @p o with a deep_copy of the object.
template<class T>
[[nodiscard]] auto& replace_with_copy(std::shared_ptr<const T>& o) {
	const auto replacement = deep_copy(o);
	o = replacement;
	return *replacement;
}

template<class T, class... P>
[[nodiscard]] auto& add_to_vec(
        std::vector<std::shared_ptr<const T>>& container,
        P... params) {
	auto val = std::make_shared<T>(std::move(params)...);
	container.emplace_back(val);
	return *val;
}

/**
 * @brief Contains the full state config system.
 * @tparam T Datatype stored in the ConfigStorage
 */
template<class T>
class ConfigStore {
public:
	constexpr static auto undo_buffer_size = 1023;

	/// Replace current configuration with latest from undo buffer
	void undo() {
		switch_buffer(undoBuffer, redoBuffer);
	}

	/// Replace current configuration with latest from redo buffer
	void redo() {
		switch_buffer(redoBuffer, undoBuffer);
	}

	/// Create a copy of the config in the undo buffer and return mutable ref.
	T& setConfig() {
		// store copy of current state in undo buffer
		undoBuffer.push_back(currentConfig);
		//then apply config change
		auto new_config = std::make_shared<T>(*currentConfig);
		currentConfig = new_config;
		// guard against undo buffer growing to much
		if (undoBuffer.size() > undo_buffer_size)
			undoBuffer.erase(undoBuffer.begin());

		redoBuffer.clear();
		return *new_config;
	}

	/// Return a const reference to the current configuration.
	const T& getConfig() const {
		return *currentConfig;
	}

private:
	std::shared_ptr<const T> currentConfig = std::make_shared<T>();
	std::vector<std::shared_ptr<const T>> undoBuffer{};
	std::vector<std::shared_ptr<const T>> redoBuffer{};

	void switch_buffer(
	        std::vector<std::shared_ptr<const T>>& from,
	        std::vector<std::shared_ptr<const T>>& to) {
		if (from.empty())
			return;

		to.emplace_back(currentConfig);
		currentConfig = from.back();
		from.pop_back();
	}
};

#endif//LIGHTCONFIG_CONFIGSTORE_H
