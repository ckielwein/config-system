# Config System Prototype

A config system, which stores configuration state in reference counts
to achieve full undo and redo capabilities.

Stored config objets are immutable.
Before every change, the root config is copied, creating a shallow copy of the full state.
To change a configuration object, the old object is copied and a mutable reference to the copy is returned to the user.
