#ifndef LIGHTCONFIG_LIGHTCONFIG_H
#define LIGHTCONFIG_LIGHTCONFIG_H

#include "ConfigStore.h"

#include <cassert>
#include <memory>
#include <vector>

//as dummy types for this prototype, all actual configs are just primitives
using EffectConfig = float;
using TimeTick = int;
using Fixture = int;

struct CueConfig {
	std::shared_ptr<const TimeTick> duration = std::make_shared<TimeTick>();
	std::shared_ptr<const std::vector<const Fixture*>> fixtures
	        = std::make_shared<const std::vector<const Fixture*>>();
};

/// Configuration of a single cue.
class Cue {
public:
	explicit Cue(std::shared_ptr<const CueConfig> c) : config(std::move(c)) {}

	Cue() = delete;

	[[nodiscard]] CueConfig& setConfig() {
		return replace_with_copy(config);
	}

	[[nodiscard]] EffectConfig& setEffect() {
		return replace_with_copy(effect);
	}

	void addFixture(Fixture* fix) {
		auto& fixtures = replace_with_copy(setConfig().fixtures);
		fixtures.emplace_back(fix);
	}

	[[nodiscard]] const CueConfig& getConfig() const {
		return *config;
	}
	[[nodiscard]] const EffectConfig& getEffect() const {
		return *effect;
	}

private:
	std::shared_ptr<const CueConfig> config;
	std::shared_ptr<const EffectConfig> effect
	        = std::make_shared<EffectConfig>();
};

/// Configuration of a single cuelist
class CueList {
public:
	explicit CueList(std::shared_ptr<CueConfig> c)
	  : defaultConfig(std::move(c)) {
		assert(defaultConfig);
	}

	Cue& addCue() {
		return add_to_vec(cues, defaultConfig);
	}

	[[nodiscard]] Cue& setCue(std::size_t index) {
		return replace_with_copy(cues[index]);
	}

	[[nodiscard]] const Cue& getCue(std::size_t index) const {
		return *cues[index];
	}

private:
	std::shared_ptr<const CueConfig> defaultConfig;
	std::vector<std::shared_ptr<const Cue>> cues;
};

/// The Class storing the full show configuration
class Show {
public:
	[[nodiscard]] auto& setCueLists() {
		return conf.setConfig().cueLists;
	}

	[[nodiscard]] auto& setFixtures() {
		return conf.setConfig().fixtures;
	}

	[[nodiscard]] const auto& getCueLists() const {
		return conf.getConfig().cueLists;
	}

	[[nodiscard]] const auto& getFixtures() const {
		return conf.getConfig().fixtures;
	}

	CueList& addCuelist() {
		auto& config = conf.setConfig();
		return add_to_vec(config.cueLists, *config.defaultCueList);
	}

	Fixture* addFixture(Fixture fix) {
		auto& config = conf.setConfig();
		return &add_to_vec(config.fixtures, fix);
	}

	[[nodiscard]] CueList& setCueList(std::size_t index) {
		auto& config = conf.setConfig();
		return replace_with_copy(config.cueLists[index]);
	}

	void redo() {
		conf.redo();
	}

	void undo() {
		conf.undo();
	}

private:
	struct ShowConfig {
		std::shared_ptr<const CueList> defaultCueList
		        = std::make_shared<CueList>(std::make_shared<CueConfig>());
		std::vector<std::shared_ptr<const CueList>> cueLists{};
		std::vector<std::shared_ptr<const Fixture>> fixtures{};
	};

	ConfigStore<ShowConfig> conf;
};

#endif//LIGHTCONFIG_LIGHTCONFIG_H
